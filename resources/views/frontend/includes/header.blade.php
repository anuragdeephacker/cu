<!---HEADER---->

<!--DROPDOWN BUTTON-->
<section class="menu cid-rFjEQOGe8D" once="menu" id="menu1-n">
    <nav class="navbar navbar-expand beta-menu navbar-dropdown align-items-center navbar-fixed-top navbar-toggleable-sm">
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <div class="hamburger">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    </div>
    </button>
    
    <!--BRAND LOGO-->
    <div class="menu-logo">
    <div class="navbar-brand">
    <span class="navbar-logo">
    <a href="{{route('home')}}">
      <img src="images/coupons_warrior_logo.png" alt="#" title="" style="height: 4.2rem;">
    </a>
    </span></div></div>
    
    <!--NAVBAR ITEMS-->
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav nav-dropdown" data-app-modern-menu="true">
    <li class="nav-item dropdown">
      <a class="nav-link link text-white dropdown-toggle display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">
       	Select Country
      </a>
		<div class="dropdown-menu">
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Malaysia') ])}}">Malaysia</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Indonesia') ])}}">Indonesia</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Philippines') ])}}">Philippines</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Singapore') ])}}">Singapore</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Thailand') ])}}">Thailand</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Vietnam') ])}}">Vietnam</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Taiwan') ])}}">Taiwan</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Australia') ])}}">Australia</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Egypt') ])}}">Egypt</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Germany') ])}}">Germany</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Morocco') ])}}">Morocco</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Saudi Arabia') ])}}">Saudi Arabia</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('United Arab Emirates') ])}}">United Arab Emirates</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Kuwait') ])}}">Kuwait</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('India') ])}}">India</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Hong Kong') ])}}"> Hong Kong</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Brunei Darussalam') ])}}"> Brunei Darussalam</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('European Union') ])}}"> European Union</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('Great Britain') ])}}"> Great Britain</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('New Zealand') ])}}">New Zealand</a>
			<a class="text-white dropdown-item display-4" href="{{route('country_wise',[ 'country_name' => base64_encode('International') ])}}">International</a>



		
		</div>
    </li>
    <li class="nav-item">
      <a class="nav-link link text-white display-4" href="#" data-toggle="modal" data-target="#login-modal" aria-expanded="false">Login</a></li>
 
      <li class="nav-item dropdown">
      <a class="nav-link link text-white dropdown-toggle display-4" href="#" data-toggle="dropdown-submenu" aria-expanded="false">
       Category
      </a>
     <div class="dropdown-menu">
      <a class="text-white dropdown-item display-4" href="{{route('category',[ 'category_name' => base64_encode('Fashion') ])}}">FASHION</a>
      <a class="text-white dropdown-item display-4" href="{{route('category',[ 'category_name' => base64_encode('Health & Beauty') ])}}">HEALTH & BEAUTY</a>
      <a class="text-white dropdown-item display-4" href="{{route('category',[ 'category_name' => base64_encode('Travel') ])}}">TRAVEL</a>
      <a class="text-white dropdown-item display-4" href="{{route('category',[ 'category_name' => base64_encode('Marketplace') ])}}">MARKETPLACE</a>
      <a class="text-white dropdown-item display-4" href="{{route('category',[ 'category_name' => base64_encode('Services') ])}}">SERVICES</a>
    </div>
    </li>


  

		{{-- <div class="navbar-buttons mbr-section-btn">
			<input type="search" placeholder="Search">
			<select id="" name="title" class="form-control couponSearch">
				<option disabled selected>Type to search...</option>
			</select>
		</div> --}}
		
    </div></nav>
		<div class="navbar-buttons mbr-section-btn">
			<div class="extra-search-padding">
				{{-- <a class="btn btn-sm btn-primary display-4" href="{{route('home')}}">Home</a> --}}
				{{-- <span>Search Here..</span> --}}
				<select class="couponSearch form-control"  style="width: 160px;" name="couponSearch" >
					<option value="" class="search-placeholder" disabled selected>Search Here..</option>
				</select>
			</div>
        </div>
    </section>
    </br></br></br>
    

  
<!-- login-modal-start -->


<div id="login-modal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Login</h5>
          {{--  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>  --}}
        </div>
        <div class="modal-body">
            <form action="{{ url('/login') }}" method="POST" data-parsley-validate id="fronten_login_form">
                {{ csrf_field() }}
                <div class="form-group has-feedback{{ isset($errors) ? $errors->has('email') ? ' has-error' : '' : '' }}">
                  <label for="user-id">Username:</label>
                  <input id="email" class="" placeholder="Enter username" name="email" value="" required autofocus>
                  
                  @if (isset($errors))
                  @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    @endif
                    {{--  <span class="glyphicon glyphicon-envelope form-control-feedback"></span>  --}}
                </div>
                <div class="form-group">
                  <label for="pwd">Password:</label>
                  <input id="password" type="password" class="" placeholder="xxxxxxx" name="password" required>
                  @if (isset($errors))
                  @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
                @endif
                @endif
                
              {{--  <span class="glyphicon glyphicon-lock form-control-feedback"></span>  --}}
                </div>
                <!-- <div class="checkbox">
                  <label><input type="checkbox"> Remember me</label>
                </div> -->
                <button type="submit" id="login_submit" class="btn btn-red">Submit</button>
                <span id="error_msg"></span>
            </form>
        </div>
      </div>
  </div>
</div>

<!-- login-modal-end -->



<script type="text/javascript">


  $('.couponSearch').select2({
    placeholder: 'Type to Search..',
    ajax: {
		url: '/search-now',
		dataType: 'json',
		delay: 400,
		minimumInputLength: 2,
		data: function (params) {
					search = params.term;
					return {
						q: $.trim(params.term),
					};
				},
      // processResults: function (data) {
      //   // alert(data);
      //   return {
      //     results:  $.map(data, function (item) {
      //           return {
      //               offer_name: item.offer_name,
      //               id: item.id
      //           }
      //       })
      //   };
      // },

      processResults: function (data) {
          return {
              results: data
          };
      },
      cache: true
    },
    templateResult: format,
  });

  function format(d) {
    //   var wIcon = $('<a href=" '+d.tracking_link+' " ' + '"><option value="'+d.tracking_link+'">'+d.campaign_name+'</option></a>');
      var wIcon = $('<option value="'+d.id+'">'+d.campaign_name+'</option>');

      if (d.loading)
          return "Searching...";
      else
          return wIcon;
  }

  	$(document).on('change','.couponSearch',function () {
		var data = $(this).val();
		// alert(data);
		console.log(data);
		var coupon_detail_url = '{{ route("search-result", ['coupon_id' => ':id']) }}';
		coupon_detail_url = coupon_detail_url.replace(':id',data);
		console.log(coupon_detail_url);
		window.location.href = coupon_detail_url;
	});


</script>
