<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->string('offer_id', 255);
            $table->string('offer_name',255);
            $table->string('preview_url',255);
            $table->text('description');
            $table->string('currency',255);
            $table->string('offer_logo',255);
            $table->string('lookup_value',255);
            $table->string('validation_terms',255);
            $table->string('payment_terms',255);
            $table->timestamp('datetime_updated')->nullable();
            $table->string('countries',255);
            $table->string('tracking_link',255);
            $table->string('categories',255);
            $table->string('commission',255);

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
