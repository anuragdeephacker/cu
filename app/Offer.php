<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $table = 'offers';

    protected $fillable = [
        'preview_url', 'offer_id', 'offer_name', 'currency', 'description',
        'offer_logo', 'lookup_value', 'validation_terms', 'payment_terms', 
        'tracking_link', 'categories', 'datetime_updated', 'commission','countries',
    ];
}
