
<!--MAIN SLIDER STYLISH BLACK-->
	<!--  BODY section --><!-- add to the <body> of your page -->
  <!--LINKS START-->
  <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.js"></script>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.css"/>
  {{-- <div class="space"></div> --}}
  <!--END LINKS-->

  <div class="slider_wrapper">
		<div class="slider_container">
			<div class="owl-carousel">
		
				@foreach ($sliderCoupons as $sliderCoupon)
					<!--IMAGE -->
					<div class="item">
						<img src="{{ $sliderCoupon->banner_image_url }}">
					</div>
				@endforeach
			
				{{-- <!--IMAGE 2-->
				<div class="item">
					<img src="https://res.cloudinary.com/milairagny/image/upload/v1487938123/pexels-photo-5_x69tiz.jpg">
				</div>
			
				<!--IMAGE 3-->
				<div class="item">
					<img src="https://res.cloudinary.com/milairagny/image/upload/v1487938123/pexels-photo-5_x69tiz.jpg">
				</div>
			
				<!--IMAGE 4-->
				<div class="item">
					<img src="https://res.cloudinary.com/milairagny/image/upload/v1487938123/pexels-photo-5_x69tiz.jpg">
				</div>
			
				<!--IMAGE 5-->
				<div class="item">
					<img src="https://res.cloudinary.com/milairagny/image/upload/v1487938123/pexels-photo-5_x69tiz.jpg">
				</div>
			
				<!--IMAGE 6-->
				<div class="item">
					<img src="https://res.cloudinary.com/milairagny/image/upload/v1487938123/pexels-photo-5_x69tiz.jpg">
				</div> --}}
		
			</div>
		</div>
  </div>

  <div class="space"></div> 

  <!--]BODY section -->
<!-- End MAIN SLIDER-->



<!--CSS CODES-->
<style>
/* Click the image one by one to see the different layout */

.space{
  padding:10px;
}

.owl-prev {
  background: url('https://res.cloudinary.com/milairagny/image/upload/v1487938188/left-arrow_rlxamy.png') left center no-repeat;
  height: 54px;
  position: absolute;
  top: 50%;
  width: 27px;
  z-index: 1000;
  left: 2%;
  cursor: pointer;
  color: transparent;
  margin-top: -27px;
}

.owl-next {
  background: url('https://res.cloudinary.com/milairagny/image/upload/v1487938220/right-arrow_zwe9sf.png') right center no-repeat;
  height: 54px;
  position: absolute;
  top: 50%;
  width: 27px;
  z-index: 1000;
  right: 2%;
  cursor: pointer;
  color: transparent;
  margin-top: -27px;
}

.owl-prev:hover,
.owl-next:hover {
  opacity: 0.5;
}


/* Icon CSS */
.owl-carousel .owl-item img {
  transform-style: preserve-3d;
  border: 7px solid white;
  /* border: 7px solid red; */
  border-radius: 10px;
}

</style>
<!--CSS ENDS-->


<!--JAVASCRIPT CODES-->
<script>
$('.owl-carousel').owlCarousel({
  autoplay: true,
  autoplayTimeout: 2000,
  autoplayHoverPause: true,
  loop: true,
  margin: 50,
  responsiveClass: true,
  nav: true,
  loop: true,
  stagePadding: 100,
  responsive: {
    0: {
      items: 1
    },
    568: {
      items: 2
    },
    600: {
      items: 3
    },
    1000: {
      items: 3
    }
  }
})
$(document).ready(function() {
  $('.popup-youtube').magnificPopup({
    disableOn: 320,
    type: 'iframe',
    mainClass: 'mfp-fade',
    removalDelay: 160,
    preloader: false,
    fixedContentPos: true
  });
});
$('.item').magnificPopup({
  delegate: 'a',
});

</script>
<!-- END SCRIPT-->

          
      