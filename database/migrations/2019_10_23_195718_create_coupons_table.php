<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coupons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));

            $table->string('campaign_banner_id',255)->nullable();
            $table->string('offer_id', 255);
            $table->string('offer_name',255);
            $table->string('campaign_name',255);
            $table->string('description',255);
            $table->string('voucher_code',255);
            $table->timestamp('date_campaign_start')->nullable();
            $table->timestamp('date_campaign_end')->nullable();
            $table->string('banner_image_url',255);
            $table->string('tracking_link',255);
            $table->string('categories',255);
            $table->string('html_tracking_link',255)->nullable();
            $table->string('offered_company',255);

            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->softDeletes();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupons');
    }
}
