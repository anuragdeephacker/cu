<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $table = 'coupons';

    protected $fillable = [
        'campaign_banner_id', 'offer_id', 'offer_name', 'campaign_name', 'description',
        'voucher_code', 'date_campaign_start', 'date_campaign_end', 'banner_image_url', 
        'tracking_link', 'categories', 'html_tracking_link', 'offered_company',
    ];
}
