<!--TEXT BLOCK-->
<section class="mbr-section info2 cid-rFjxW7qZrm" id="info2-9">
    <div class="container">
    <div class="row main justify-content-center">
    <div class="media-container-column col-12 col-lg-3 col-md-4">
    <div class="mbr-section-btn align-left py-4"><a class="btn btn-secondary display-4" href="page4.html#content5-2v">
    {{-- <span class="mbri-preview mbr-iconfont"></span> --}}
    BROWSE ALL COUPONS
    </a></div></div>
    <div class="media-container-column title col-12 col-lg-7 col-md-6">
    <h2 class="align-right mbr-bold mbr-white pb-3 mbr-fonts-style display-2">CouponsWarrior</h2>
    <h3 class="mbr-section-subtitle align-right mbr-light mbr-white mbr-fonts-style display-5">World of working and latest coupons</h3>
    </div>
    </div>
    </div>
    </section>
    
    
      
    <!--Left Side Buttons Start-->	
    <div class="sticky-left-container">
    <ul class="sticky-left">
    <li>
    <img width="32" height="32" title="" alt="" src="https://drive.google.com/uc?id=1yz1jMRdqLQWBx10eFLSWTMGWmKrL9sM6"/>
    <p>Phone</p>
    </li>
    <li>
    <img width="32" height="32" title="" alt="" src="https://drive.google.com/uc?id=1ha6UAQlGBFXnExIXUMgs2l1QwUNgE09K" />
    <p>Whatsapp</p>
    </li>
    <li>
    <img width="32" height="32" title="" alt="" src="https://drive.google.com/uc?id=1Jthmdc-CZpBoymeHEW06x9OTj8_lW7gM" />
    <p>Email</p>
    </li>
    </ul>
    </div>
    <!--Left Side Buttons End-->
    
    
    
    <!--FOOTER SECTION-->
    <section class="cid-qTkAaeaxX5" id="footer1-2">
    <div class="container">
    <div class="media-container-row footer-content text-white">
    <div class="col-12 col-md-3">
    <div class="media-wrap">
    <a href="index.html">
    <!--FOOTER LOGO AND LINK-->
    <img src="#" alt="" title=""></a>
    </div></div>
    <!--ADDRESS-->
    {{-- <div class="col-12 col-md-3 mbr-fonts-style display-7">
    <h5 class="pb-3"><strong>
        Address
    </strong></h5>
    <p class="mbr-text">
        1234 Street&nbsp;
        <br>Delhi, India</p>
    </div> --}}
    
    {{-- <div class="col-12 col-md-3 mbr-fonts-style display-7">
    <h5 class="pb-3"><strong>
    <a href="index.html">Contacts</a>
    </strong></h5> --}}
    {{-- <p class="mbr-text">
        Email: support@couponswarrior.com
    <br>Phone: +91 000 0000 001
    <br>Fax: +91 000 0000 002
    </p> --}}
    </div>
    <div class="col-12 col-md-3 mbr-fonts-style display-7">
    <h5 class="pb-3"><strong>
        Links
    </strong></h5>
    <p class="mbr-text"><a href="index.html">Home</a></p>
    </div>
    </div>
    <div class="footer-lower">
    <div class="media-container-row">
    <div class="col-sm-12">
    <hr>
    </div></div>
    <!--COPYRIGHT-->
    <div class="media-container-row mbr-white">
    <div class="col-sm-6 copyright">
    <p class="mbr-text mbr-fonts-style display-7">
        © Copyright 2019 CouponsWarrior - All Rights Reserved
    </p></div>
    <div class="col-md-6">       
    </div></div></div></div></div></section>