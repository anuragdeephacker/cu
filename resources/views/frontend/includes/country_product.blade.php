
<div class="container">

	{{-- QUICK NAVIGATION  --}}
	<div class="quick-navigation">
		<span class="navigation-text"><a href=" {{ route('home') }}">Home </a>/{{-- / Category / --}} {{ $countryName }}</span>
	</div>
	
	{{-- CATEGORY NAME  --}}
	<div class="category-name">
		<span class="text1">{{ $countryName }}</span>
	</div>

	<div class="row product-list">
		@if (count($countryWiseOffers) > 0)
            
            @foreach ($countryWiseOffers as $offer)
            <div class="col-sm-4 mb-3 product-box">
                {{-- START OF CATEGORY --}}
                <div class="col-md-4 ">
                    
                    {{-- @for ($i = 0; $i < $offer->offersCount; $i++) --}}
                        
                        {{-- @if ($i == 0) --}}
                            {{-- START OF BIGGER PRODUCT  --}}
                            <div class="row bigger-product">
                                <div class="wrapper">
                                    {{-- <div class="offer-name">
                                        {{ $offer->offers_array[$i]->offer_name }}
                                    </div> --}}

                                    <figure class="wow animated portfolio-item game-cate-each" data-wow-duration="500ms" data-wow-delay="0ms">
                                        <img src="{{ $offer->offer_logo }}"  class="bigbox "/>
                                        <div class="overlay">
                                            <div class="content"><a target="_blank" href="{{ $offer->tracking_link }}">GET COUPON</a></div>
                                        </div>
                                    </figure>
                                </div>
                            </div>
                            {{-- END OF BIGGER PRODUCT  --}}

                            
                        {{-- @endif --}}
                        
                        
                    {{-- @endfor --}}
                
                </div>

                {{-- END OF CATEGORY  --}}
            </div>
            @endforeach
        @endif
        
    </div>

    <input type="hidden" class="country_name" value="{{  $countryName  }}">
    
    @if (count($countryWiseOffers) > 0)
        {{-- //VIEW ALL COUPONS BUTTON  --}}
        <div class="mbr-section-btn align-center py-4">
            <a class="btn custom-btn btn-secondary display-4 load-more" data-totalResult="{{ $totalCounts }}" href="javascript:">
                VIEW MORE
            </a>
        </div>
    @endif

</div>



{{-- Ajax Script Start --}}
{{-- <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script> --}}
<script type="text/javascript">
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".load-more").on('click',function(){
            var _totalCurrentResult=$(".product-box").length;
            // var _totalCurrentResult = find(".product-box").val(total);
            var country_name=$(".country_name").val();
            var main_site="/";
            // alert(main_site);
            // Ajax Reuqest
            $.ajax({
                url:'/load-more-offer',
                type:'get',
                dataType:'json',
                // async : false ,
                data:{
                    skip:_totalCurrentResult,
                    country_name:country_name
                },

                beforeSend:function(){
                    $(".load-more").html('Loading...');
                },
                success:function(response){
                    // _html += response;
                    // var image="{{ asset('imgs') }}/";
                    $.each(response,function(index,offer){
                        // alert(value.tracking_link);
                        var _html='';

                        _html+='<div class="col-sm-4 mb-3 product-box">';
                       
                            _html+='<div class="col-md-4">';
                                _html+='<div class="row bigger-product">';
                                    _html+='<div class="wrapper">';
                                        

                                        _html+= '<figure class="wow animated portfolio-item game-cate-each" data-wow-duration="500ms" data-wow-delay="0ms">';
                                            _html+='<img src=" '+offer.offer_logo+' "  class="bigbox "/>';
                                            
                                            // Space for overlay
                                        _html+='</figure>';
                                    _html+='</div>';
                                _html+='</div>';
                            _html+='</div>';
                        _html+='</div>';
                        
                        $(".product-list").append(_html);
                    });

                    // Change Load More When No Further result
                    var _totalCurrentResult=$(".product-box").length;
                    var _totalResult=parseInt($(".load-more").attr('data-totalResult'));
                    console.log(_totalCurrentResult);
                    console.log(_totalResult);
                    if(_totalCurrentResult == _totalResult){
                        $(".load-more").remove();
                    }else{
                        $(".load-more").html('Load More');
                    }
                }
            });
        });
    });
</script>
{{-- Ajax Script End --}}