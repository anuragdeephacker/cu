
<!--MAIN SLIDER STYLISH BLACK-->
	<!--  BODY section --><!-- add to the <body> of your page -->
  <div class="space"></div>
  <!--END LINKS-->

  <div class="category_image_wrapper">
		<div class="category_image_container">
			@if ($categoryName == "Health & Beauty")
				<img src="/images/beauty.jpeg" alt="HEALTH & BEAUTY">
			@elseif ($categoryName == "Fashion")
				<img src="/images/fashion.jpeg" alt="FASHION">
			@elseif ($categoryName == "Travel")
				<img src="/images/travels.jpeg" alt="TRAVELS">
			@elseif ($categoryName == "Marketplace")
				<img src="/images/marketplace.jpeg" alt="MARKETPLACE">
			@elseif ($categoryName == "Services")
				<img src="/images/services.jpeg" alt="SERVICES">
			@endif
		</div>
  </div>
  

  <div class="space"></div> 

<!-- End MAIN SLIDER-->

          
      