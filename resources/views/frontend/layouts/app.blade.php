<!DOCTYPE html>
<html lang="en">
<head>
<title>CouponsWarrior.com | A Hub For Discount Coupons</title>

<!-- META SECTION -->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
<link rel="shortcut icon" href="assets/images/wallpaper2you-546056-96x60.jpeg" type="image/x-icon">
<meta name="description" content="">

{{-- CSS --}}

<link rel="stylesheet" href="{{ url('css/play_style.css') }}">
<link rel="stylesheet" href="{{ url('css/play_main.css') }}">


<link rel="stylesheet" href="{{ url('assets/web/assets/mobirise-icons/mobirise-icons.css') }}">
<link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap-grid.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/bootstrap/css/bootstrap-reboot.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/socicon/css/styles.css') }}">
<link rel="stylesheet" href="{{ url('assets/tether/tether.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/animatecss/animate.min.css') }}">
<link rel="stylesheet" href="{{ url('assets/dropdown/css/style.css') }}">
<link rel="stylesheet" href="{{ url('assets/theme/css/style.css') }}">
<link rel="preload" as="style" href="{{ url('assets/mobirise/css/mbr-additional.css') }}">
<link rel="stylesheet" href="{{ url('assets/mobirise/css/mbr-additional.css') }}" type="text/css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lato:400">
<link rel="stylesheet" href="{{ url('assets/social/css.css') }}">

<link rel="stylesheet" href="{{ url('css/custom.css') }}">

<!--// css -->
{{-- SEARCH CDN --}}
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
 
  
<!-- Start WOWSlider.com HEAD section --> <!-- add to the <head> of your page -->
<link rel="stylesheet" type="text/css" href="engine1/style.css" />
<script type="text/javascript" src="engine1/jquery.js"></script>
<!-- End WOWSlider.com HEAD section -->

{{-- //custom css --}}
<link rel="stylesheet" href="{{ url('css/custom.css') }}">






{{-- javascript files --}}
<!--JAVASCRIPT FILES-->
<script src="assets/web/assets/jquery/jquery.min.js"></script>
<script src="assets/popper/popper.min.js"></script>
<script src="assets/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/smoothscroll/smooth-scroll.js"></script>
<script src="assets/ytplayer/jquery.mb.ytplayer.min.js"></script>
<script src="assets/vimeoplayer/jquery.mb.vimeo_player.js"></script>
<script src="assets/tether/tether.min.js"></script>
<script src="assets/bootstrapcarouselswipe/bootstrap-carousel-swipe.js"></script>
<script src="assets/viewportchecker/jquery.viewportchecker.js"></script>
<script src="assets/dropdown/js/nav-dropdown.js"></script>
<script src="assets/dropdown/js/navbar-dropdown.js"></script>
<script src="assets/touchswipe/jquery.touch-swipe.min.js"></script>
<script src="assets/theme/js/script.js"></script>
<script src="assets/slidervideo/script.js"></script>

{{-- SEARCH CDN --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>



<!-- font -->
<link href='//fonts.googleapis.com/css?family=Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
<!-- //font -->


</head>
<body> 

    <div>
        @yield('content')
    </div>
</body>
</html>
