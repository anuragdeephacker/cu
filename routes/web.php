<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/category{category_name}', 'HomeController@listCategoryProducts')->name('category');
Route::get('/offers{country_name}', 'HomeController@listCountryOffers')->name('country_wise');
// Route::post('/login', 'HomeController@doLogin')->name('country_wise');




// Ajax Data
Route::get('load-more-data','HomeController@loadMoreData');
Route::get('load-more-offer','HomeController@loadMoreOffer');


// SEARCH AJAX 
// Route::get('search', 'HomeController@searchAjax');
Route::get('search-now', 'HomeController@searchAjax')->name('search-now');

Route::get('search-product{coupon_id}', 'HomeController@searchResult')->name('search-result');

//GET OFFERS API
Route::get('get-offers/{page_no}', 'HomeController@getOffers')->name('get-offers');

