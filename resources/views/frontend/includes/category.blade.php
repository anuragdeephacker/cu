
<div class="container">

	<div class="row">

		@foreach ($categoryWiseArray as $categoryList)
			
			{{-- START OF CATEGORY --}}
			<div class="col-md-4">
				{{-- CATEGORY NAME  --}}
	  <span class="text1">{{ $categoryList->category_name }}</span>
				
				@for ($i = 0; $i < $categoryList->couponsCount; $i++)
					
					@if ($i == 0)
						{{-- START OF BIGGER PRODUCT  --}}
						<div class="row bigger-product">
							<div class="wrapper">
								{{-- <div class="offer-name">
									{{ $categoryList->coupons_array[$i]->offer_name }}
								</div> --}}

								<figure class="wow animated portfolio-item game-cate-each" data-wow-duration="500ms" data-wow-delay="0ms">
									<img src="{{ $categoryList->coupons_array[$i]->banner_image_url }} "  class="bigbox "/>
									{{-- <div class="overlay">
										<div class="offer-description">
											{{ $categoryList->coupons_array[$i]->description }}
										</div>
										<div class="content"><a target="_blank" href="{{ $categoryList->coupons_array[$i]->tracking_link }}">GET COUPON</a></div>
									</div> --}}
								</figure>
							</div>
						</div>
						{{-- END OF BIGGER PRODUCT  --}}
					@else
						{{-- START OF SMALLER PRODUCT  2 TIMES--}}
						@for ($i = 1; $i < $categoryList->couponsCount; $i++)
							@if ($i == 1 || $i == 3)
								<div class="row smaller-product">

									@for ($k = $i; $k < $i+2; $k++)
									
										<div class="col-md-6">
											<div class="row">
												<div class="wrapper">
													<figure class="wow animated portfolio-item game-cate-each" data-wow-duration="500ms" data-wow-delay="0ms">
														<div class="box">
															<center>
																<img src="{{ $categoryList->coupons_array[$k]->banner_image_url }}" style="max-width:100%;max-height:100%;height:auto;width:auto;vertical-align: middle;text-align: center;"/>
																{{-- <img src="https://img.involve.asia/rpss/campaigns_banners/36018-eKL5uNQMiHIRKk7yK4uc1QDVkGwCytWq.png" style="max-width:100%;max-height:100%;height:auto;width:auto;vertical-align: middle;text-align: center;"/> --}}
															</center>
															<div class="overlay">
																<div class="content"><a target="_blank" href="{{ $categoryList->coupons_array[$k]->tracking_link }}">GET COUPON</a></div>
															</div>
														</div>
													</figure>
												</div>
											</div>
										</div>
	
									@endfor

								</div>
							@endif
						@endfor
						{{-- END OF SMALLER PRODUCT  --}}
					@endif
					
					
				@endfor

				{{-- //VIEW ALL COUPONS BUTTON  --}}
				<div class="mbr-section-btn align-center py-4">
					<a class="btn custom-btn btn-secondary display-4" href="{{route('category',[ 'category_name' => base64_encode($categoryList->category_name) ])}}">
						VIEW ALL COUPONS
					</a>
				</div>
			
			</div>

			{{-- END OF CATEGORY  --}}
		@endforeach
		
	</div>

</div>
	
</div>