@extends('frontend.layouts.app')

@section('content')
    <div class="">
        <div class="banner" id="home">
            @include('frontend.includes.header')
        </div>
        @include('frontend.includes.slider')

        <div class="categories" id="categories">
            @include('frontend.includes.category')
        </div>

        @include('frontend.includes.footer')
    </div>

@endsection