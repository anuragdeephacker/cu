<?php

namespace App\Http\Controllers;
use Log;
use App\Coupon;
use App\Offer;
use GuzzleHttp\Client;
use Illuminate\Http\Request;



class HomeController extends Controller
{
    public function index()
    {
        try {
            $sliderCoupons = Coupon::where('voucher_code','!=','')
                                        ->where('tracking_link','!=','')
                                        ->where('banner_image_url','!=','')
                                        ->where('banner_image_url','!=','https://img.involve.asia/rpss/campaigns_banners/no-img.jpg')
                                        ->inRandomOrder()
                                        ->limit(6)
                                        ->get(); //->pluck('banner_image_url', 'id')

            // $homeCategory = Category::where('is_shown_in_home',1)->orderBy('position')->get();
            $homeCategory = array('Fashion','Health & Beauty','Travel','Marketplace','Services');

            $categoryWiseArray = [];
            foreach( $homeCategory as $category ) {
                $coupons = Coupon::where('categories',$category )
                                        ->where('tracking_link','!=','')
                                        ->where('banner_image_url','!=','https://img.involve.asia/rpss/campaigns_banners/no-img.jpg')
                                        ->inRandomOrder()
                                        ->take(5)
                                        ->get();

                $newOb = new \stdClass();
                $newOb->category_name = $category;
                // $newOb->category_image = $category->cat_image;
                $newOb->couponsCount = count($coupons);
                $newOb->coupons_array = $coupons;

                if($newOb->couponsCount>0)
                    array_push($categoryWiseArray,$newOb);
            }
            // dd($sliderCoupons);
            return view('frontend.pages.index',compact('sliderCoupons','categoryWiseArray'));
        }
        catch ( \Exception $e ) {
            Log::error ( " :: EXCEPTION :: ".$e->getMessage()."\n".$e->getTraceAsString() );
            abort(500);
        }
    }

    public function listCategoryProducts(Request $request)
    {
        try {
            $categoryName = base64_decode($request->category_name);
            // dd($categoryName);
            $categoryWiseCoupons = Coupon::where('categories',$categoryName )
                                            ->where('banner_image_url','!=','https://img.involve.asia/rpss/campaigns_banners/no-img.jpg')
                                            ->take(6)->get(); //->pluck('banner_image_url', 'id')
            $couponsCount = count($categoryWiseCoupons);
            // $couponsCount = "";

            $totalCounts = Coupon::where('categories',$categoryName )->count();
            
            // dd($categoryWiseCoupons);
            return view('frontend.pages.categoryWise',compact('categoryWiseCoupons','categoryName','totalCounts'));
        }
        catch ( \Exception $e ) {
            Log::error ( " :: EXCEPTION :: ".$e->getMessage()."\n".$e->getTraceAsString() );
            abort(500);
        }
    }


    // Fetch More Data
    function loadMoreData(Request $request){
        // dd($categoryName);
        if($request->ajax()){
            $skip=$request->skip;
            $categoryName = $request->category_name;
            Log::debug ( " :: CATEGORY NAME IS :: " .$categoryName );
            Log::debug ( " :: SKIP :: " .$skip );

            $take=3;
            $products=Coupon::where('categories',$categoryName )
                                ->where('banner_image_url','!=','https://img.involve.asia/rpss/campaigns_banners/no-img.jpg')
                                ->skip($skip)->take($take)->get();
            // return response()->json($products);
            Log::debug ( " :: PRODUCTS :: " .$products );

            return response()->json($products);

        }else{
            return response()->json('Direct Access Not Allowed!!');
        }
    }

    // SEARCH FUNCTION 
    public function searchAjax(Request $request)
    {
        $data = [];
        
        $formatted_tags = [];
        
        if($request->has('q')){
            $search = $request->q;
            Log::debug ( " :: SEARCH QUERY IS :: " .$search );
            if (empty($search)) {
                return response()->json();
            }
            $coupons = Coupon::select('coupons.id','coupons.offer_name','coupons.campaign_name','coupons.tracking_link')
                   ->where('offer_name','LIKE',"%{$search}%")
                   ->orWhere('description','LIKE',"%{$search}%")
                   ->orWhere('campaign_name','LIKE',"%{$search}%")
                   ->where('banner_image_url','!=','https://img.involve.asia/rpss/campaigns_banners/no-img.jpg')
                //    ->groupBy('coupons.id','coupons.offer_name')
                   ->limit(10)
                   ->get();
            // $coupons = Coupon::take(5)->get();
                // dd($coupons);
            
                Log::debug ( " :: COUPONS :: " .$coupons );
            
            foreach ($coupons as $coupon) {
                $formatted_tags[] = [
                    'id' => $coupon->id,
                    'offer_name' => $coupon->offer_name,
                    'campaign_name' => $coupon->campaign_name,
                    'tracking_link' => $coupon->tracking_link,
                ];
            }
            // $data = DB::table("categories")
            // 		->select("id","name")
            // 		->where('name','LIKE',"%$search%")
            // 		->get();
        }
        Log::debug ( " :: FORMATED TAGS :: " . print_r($formatted_tags,true) );

        return response()->json($formatted_tags);
    }
    
    public function searchResult(Request $request)
    {
        try {
            $coupon_id = $request->coupon_id;
            // dd($categoryName);
            Log::debug ( " :: Coupon Id :: " . print_r($coupon_id,true) );

            $coupon = Coupon::where('id',$coupon_id )->first(); //->pluck('banner_image_url', 'id')
            
            // $couponsCount = "";
            Log::debug ( " :: Searched Product :: " . print_r($coupon,true) );
            
            // dd($categoryWiseCoupons);
            return view('frontend.pages.searchResult',compact('coupon'));
        }
        catch ( \Exception $e ) {
            Log::error ( " :: EXCEPTION :: ".$e->getMessage()."\n".$e->getTraceAsString() );
            abort(500);
        }
    }

    public function getOffers(Request $request)
    {
        try {
            $page_no = $request->page_no;
            // $client = new Client([
            //     'headers' => [ 'Content-Type' => 'application/json' ],
            //     'body'    => array(
            //                         'secret' => '0PdY0yOkG/0f6yM1m7JJaJUcl0XVScQUyF0GbNul9ao=',
            //                         'key' => 'genaral'
            //                     ),
            // ]);

            // $response = Guzzle::post('https://api.involve.asia/api/authenticate', array(
            //     'headers' => [ 'Content-Type' => 'application/json' ],
            //     'body'    => array(
            //                         'secret' => '0PdY0yOkG/0f6yM1m7JJaJUcl0XVScQUyF0GbNul9ao=',
            //                         'key' => 'genaral'
            //                     ),
            // ));
            
            $client = new Client([
                'headers' => [ 'Content-Type' => 'application/json' ]
            ]);
            
            $response = $client->post('https://api.involve.asia/api/authenticate',[
                    'form_params' => [
                                'secret' => '0PdY0yOkG/0f6yM1m7JJaJUcl0XVScQUyF0GbNul9ao=',
                                'key' => 'general'
                    ]
            ]);

            // $response = $client->post('https://api.involve.asia/api/authenticate');

            $authenticate_response = json_decode($response->getBody()->getContents());
            $status = $authenticate_response->status;
            $auth_token = $authenticate_response->data->token;

            Log::debug ( " :: authenticate_response :: " . print_r($authenticate_response,true) );
            Log::debug ( " :: status :: " . print_r($status,true) );
            Log::debug ( " :: token :: " . print_r($auth_token,true) );
            Log::debug ( " :: Page No :: " . print_r($page_no,true) );


            if ($status == 'success') {
                //call offer api

                $client = new Client([
                    'headers' => [ 
                                    'Content-Type' => 'application/json',
                                    'Authorization' => 'Bearer ' . $auth_token, 
                                ],
                    
                ]);
                
                $response = $client->post('https://api.involve.asia/api/offers/all',[
                        'form_params' => [
                            "page" => $page_no,
                            "limit" => "20",
                            "sort_by" => "relevance",
                            "filters" => [
                              "offer_type" => "cpa|cps|cpa_both|cpc|cpm",
                              "categories" => "Electronics|Fashion|Finance|Health & Beauty|Lifestyle|Marketplace|Other|Services|Travel"
                            ]
                        ]
                ]);

                $offer_response = json_decode($response->getBody()->getContents());
                print_r($offer_response);
                foreach ($offer_response->data->data as $offer) {
                    // $commission = $offer->commissions[0];
                    // $commission_one = $commission->Commission;
                    // print_r($commission_one);

                    // dd($offer);
                    Offer::create([
                        'offer_id' => $offer->offer_id,
                        'offer_name' => $offer->offer_name,
                        'currency' => $offer->currency,
                        'description' => $offer->description,
                        'preview_url' => $offer->preview_url,
                        'offer_logo' => $offer->logo,
                        'lookup_value' => $offer->lookup_value,
                        'validation_terms' => $offer->validation_terms,
                        'payment_terms' => $offer->payment_terms,
                        'tracking_link' => $offer->tracking_link,
                        'categories' => $offer->categories,
                        'datetime_updated' => $offer->datetime_updated,
                        'commission' => '',
                        'countries' => $offer->countries,
                    ]);

                }
                Log::debug ( " :: offer_response :: " . print_r($offer_response,true) );


            } else {
                dd("Authenticaion Failed.. No response! Status FAILURE");
            }
            


        }
        catch ( \Exception $e ) {
            Log::error ( " :: EXCEPTION :: ".$e->getMessage()."\n".$e->getTraceAsString() );
            abort(500);
        }
    }

    public function listCountryOffers(Request $request)
    {
        try {
            $countryName = base64_decode($request->country_name);
            // dd($countryName);
            // $countryWiseOffers = Offer::where('countries',$countryName )->take(6)->get(); //->pluck('banner_image_url', 'id')
            $countryWiseOffers = Offer::where('countries','LIKE',"%{$countryName}%")
                //    ->groupBy('coupons.id','coupons.offer_name')
                   ->limit(9)
                   ->get();
            // $totalCounts = count($countryWiseOffers);
            // $offersCount = "";

            $totalCounts = Offer::where('countries','LIKE',"%{$countryName}%")->count();
            
            // dd($countryWiseOffers);
            return view('frontend.pages.countryWise',compact('countryWiseOffers','countryName','totalCounts'));
        }
        catch ( \Exception $e ) {
            Log::error ( " :: EXCEPTION :: ".$e->getMessage()."\n".$e->getTraceAsString() );
            abort(500);
        }
    }

    // Fetch More Countrywise offer
    function loadMoreOffer(Request $request){
        // dd($categoryName);
        if($request->ajax()){
            $skip=$request->skip;
            $countryName = $request->country_name;
            Log::debug ( " :: COUNTRY NAME IS :: " .$countryName );
            Log::debug ( " :: SKIP :: " .$skip );

            $take=3;
            $products=Offer::where('countries','LIKE',"%{$countryName}%")->skip($skip)->take($take)->get();
            // return response()->json($products);
            Log::debug ( " :: OFFERS :: " .$products );

            return response()->json($products);

        }else{
            return response()->json('Direct Access Not Allowed!!');
        }
    }
}
